package org.tensorflow.demo;

import android.graphics.Bitmap;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hamii on 12/28/2017.
 */

public class DistanceCalculator {

    Bitmap bitmap;
    ImageView img;
    static List<String> bigItem=new ArrayList<>();
    static List<String>smallItem=new ArrayList<>();
    static List<String>mediumItem=new ArrayList<>();




    public static void distanceCalc(String title,float height){

        System.out.println("distance cal");
        double height2 = 0;
        int distance =0;
        ListInit();

        if(bigItem.contains(title)){


            if(height > 220){

                CameraActivity.tts.speak(title+" is 1 to 2 steps away", TextToSpeech.QUEUE_ADD, null, "DEFAULT");
            }
            else if(height >160){

                CameraActivity.tts.speak(title+" is 3 to 5 steps away", TextToSpeech.QUEUE_ADD, null, "DEFAULT");
            }
            else if(height >120){

                CameraActivity.tts.speak(title+" is 5 to 8 steps away", TextToSpeech.QUEUE_ADD, null, "DEFAULT");
            }
            else if(height >90){

                CameraActivity.tts.speak(title+" is 8 to 11 steps away", TextToSpeech.QUEUE_ADD, null, "DEFAULT");
            }
            else{

                CameraActivity.tts.speak(title+" is more than 11 steps away", TextToSpeech.QUEUE_ADD, null, "DEFAULT");
            }



            if(height<=150){
                height2=height-((height*0.05));

                distance= (int) (1/(1-(height2/height)));
            }
            else {
                height2=height-((height*0.2));

                distance= (int) (1/(1-(height2/height)));
            }

        }
        else if(mediumItem.contains(title)){

            if(height > 200){

                CameraActivity.tts.speak(title+" is 1 to 2 steps away", TextToSpeech.QUEUE_ADD, null, "DEFAULT");
            }
            else if(height >140){

                CameraActivity.tts.speak(title+" is 2 to 4 steps away", TextToSpeech.QUEUE_ADD, null, "DEFAULT");
            }
            else if(height >90){

                CameraActivity.tts.speak(title+" is 5 to 6 steps away", TextToSpeech.QUEUE_ADD, null, "DEFAULT");
            }
            else if(height >60){

                CameraActivity.tts.speak(title+" is  6 to 8 steps away", TextToSpeech.QUEUE_ADD, null, "DEFAULT");
            }
            else {

                CameraActivity.tts.speak(title+" is more than 8 steps away", TextToSpeech.QUEUE_ADD, null, "DEFAULT");
            }

             if(height<=150){
                height2=height-((height*0.2));

                distance= (int) (1/(1-(height2/height)));
            }
            else {
                height2=height-((height*0.5));

                distance= (int) (1/(1-(height2/height)));
            }
        }
        else if(smallItem.contains(title)){

            if(height > 100){

                CameraActivity.tts.speak(title+" is 1 to 2 steps away", TextToSpeech.QUEUE_ADD, null, "DEFAULT");
            }
            else if(height> 60){

                CameraActivity.tts.speak(title+" is 3 to 5 steps away", TextToSpeech.QUEUE_ADD, null, "DEFAULT");
            }
            else{
                CameraActivity.tts.speak(title+" is more than 5 steps away", TextToSpeech.QUEUE_ADD, null, "DEFAULT");
            }

        }

        


    }










    static void ListInit(){

        smallItem.add("bird");
        smallItem.add("tie");
        smallItem.add("frisbee");
        smallItem.add("skis");
        smallItem.add("sports ball");
        smallItem.add("kite");
        smallItem.add("baseball glove");
        smallItem.add("bottle");
        smallItem.add("wine glass");
        smallItem.add("cup");
        smallItem.add("fork");
        smallItem.add("knife");
        smallItem.add("spoon");
        smallItem.add("bowl");
        smallItem.add("banana");
        smallItem.add("apple");
        smallItem.add("sandwich");
        smallItem.add("orange");
        smallItem.add("broccoli");
        smallItem.add("carrot");
        smallItem.add("hot dog");
        smallItem.add("donut");
        smallItem.add("pizza");
        smallItem.add("cake");
        smallItem.add("book");
        smallItem.add("door");
        smallItem.add("mouse");
        smallItem.add("remote");
        smallItem.add("keyboard");
        smallItem.add("cell phone");
        smallItem.add("toaster");
        smallItem.add("clock");
        smallItem.add("scissors");
        smallItem.add("teddy bear");
        smallItem.add("hair drier");
        smallItem.add("toothbrush");
        smallItem.add("laptop");

        bigItem.add("person");
        bigItem.add("bicycle");
        bigItem.add("car");
        bigItem.add("motorcycle");
        bigItem.add("airplane");
        bigItem.add("bus");
        bigItem.add("train");
        bigItem.add("truck");
        bigItem.add("boat");
        bigItem.add("horse");
        bigItem.add("cow");
        bigItem.add("elephant");
        bigItem.add("bear");
        bigItem.add("zebra");
        bigItem.add("giraffe");
        bigItem.add("dining table");
        bigItem.add("refrigerator");
        bigItem.add("bench");
        bigItem.add("bed");

        mediumItem.add("traffic light");
        mediumItem.add("fire hydrant");
        mediumItem.add("stop sign");
        mediumItem.add("parking meter");
        mediumItem.add("cat");
        mediumItem.add("dog");
        mediumItem.add("sheep");
        mediumItem.add("backpack");
        mediumItem.add("umbrella");
        mediumItem.add("handbag");
        mediumItem.add("suitcase");
        mediumItem.add("snowboard");
        mediumItem.add("baseball bat");
        mediumItem.add("skateboard");
        mediumItem.add("surfboard");
        mediumItem.add("tennis racket");
        mediumItem.add("chair");
        mediumItem.add("couch");
        mediumItem.add("potted plant");
        mediumItem.add("microwave");
        mediumItem.add("oven");
        mediumItem.add("sink");
        mediumItem.add("vase");
        mediumItem.add("toilet");
        mediumItem.add("tv");

    }







  /*  static void ListInit(){

        bigItem.add("person");
        bigItem.add("bicycle");
        bigItem.add("car");
        bigItem.add("motorcycle");
        bigItem.add("airplane");
        bigItem.add("bus");
        bigItem.add("train");
        bigItem.add("truck");
        bigItem.add("boat");
        bigItem.add("horse");
        bigItem.add("cow");
        bigItem.add("elephant");
        bigItem.add("bear");
        bigItem.add("zebra");
        bigItem.add("giraffe");
        bigItem.add("suitcase");
        bigItem.add("surfboard");
        bigItem.add("bed");
        bigItem.add("???");
        bigItem.add("dining table");
        bigItem.add("refrigerator");


        smallItem.add("traffic light");
        smallItem.add("fire hydrant");
        smallItem.add("stop sign");
        smallItem.add("parking meter");
        smallItem.add("bench");
        smallItem.add("bird");
        smallItem.add("cat");
        smallItem.add("dog");
        smallItem.add("sheep");
        smallItem.add("backpack");
        smallItem.add("umbrella");
        smallItem.add("handbag");
        smallItem.add("tie");
        smallItem.add("frisbee");
        smallItem.add("skis");
        smallItem.add("snowboard");
        smallItem.add("sports ball");
        smallItem.add("kite");
        smallItem.add("baseball bat");
        smallItem.add("baseball glove");
        smallItem.add("skateboard");
        smallItem.add("tennis racket");
        smallItem.add("bottle");
        smallItem.add("wine glass");
        smallItem.add("cup");
        smallItem.add("fork");
        smallItem.add("knife");
        smallItem.add("spoon");
        smallItem.add("bowl");
        smallItem.add("banana");
        smallItem.add("apple");
        smallItem.add("sandwich");
        smallItem.add("orange");
        smallItem.add("broccoli");
        smallItem.add("carrot");
        smallItem.add("hot dog");
        smallItem.add("pizza");
        smallItem.add("donut");
        smallItem.add("cake");
        smallItem.add("chair");
        smallItem.add("couch");
        smallItem.add("potted plant");
        smallItem.add("toilet");
        smallItem.add("door");
        smallItem.add("tv");
        smallItem.add("laptop");
        smallItem.add("mouse");
        smallItem.add("remote");
        smallItem.add("keyboard");
        smallItem.add("cell phone");
        smallItem.add("microwave");
        smallItem.add("toaster");
        smallItem.add("sink");
        smallItem.add("book");
        smallItem.add("clock");
        smallItem.add("vase");
        smallItem.add("scissors");
        smallItem.add("teddy bear");
        smallItem.add("hair drier");
        smallItem.add("toothbrush");

    }*/







    /*private void log(){
        // image size
        System.out.println("Condlogtructor");
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        for(int i=0; i<width; i++){
            for(int j=0; j<height; j++){


                double pixel= bitmap.getPixel(i, j);
                double m_pixel=50*Math.log(1+pixel);
                bitmap.setPixel(i,j, (int) m_pixel);


            }
        }


        // return bitmap;
        //imageView.setImage;
        //imageView.setVisibility(View.VISIBLE);

    }

    private Bitmap threshold(){

        // image size

        System.out.println("threshold");
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        for(int i=0; i<width; i++){
            for(int j=0; j<height; j++){


                double pixel= bitmap.getPixel(i, j);

                if(pixel>200){
                    bitmap.setPixel(i,j, 255);
                }
                else{
                    bitmap.setPixel(i,j, 0);
                }




            }
        }

       return bitmap;
    }

    public void clear(){

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        for(int i=0; i<width-1; i++){
            for(int j=0; j<height-1; j++){


                if((bitmap.getPixel(i, j)==0)&&(bitmap.getPixel(i, j+1)==0)&&(bitmap.getPixel(i+1, j)==0)&&(bitmap.getPixel(i+1, j+1)==0))
                {
                    bitmap.setPixel(i,j, 0);
                }
                else{
                    bitmap.setPixel(i,j, 255);
                }


            }
        }


    }

    public void height(){

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int startx=0, endx=0, height_pix;
        for(int i=0; i<width-1; i++){
            for(int j=0; j<height-1; j++){

                if(bitmap.getPixel(i, j)==0){
                    endx=i;
                    break;
                }

            }
        }

        for(int i=width-1; i>=0; i--){
            for(int j=height-1; j>=0; j--){

                if(bitmap.getPixel(i, j)==0){
                    startx=i;
                    break;
                }

            }
        }

        height_pix=startx - endx;
        System.out.println("height in pixels: "+ height_pix);
    }*/




}
