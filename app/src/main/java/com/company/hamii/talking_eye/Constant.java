package com.company.hamii.talking_eye;

/**
 * Created by Hamii on 10/22/2017.
 */

public class Constant {

    public static String DatabaseName="User_db";
    public static String TableName="personName";
    public static String Column_Ratio1="r1";
    public static String Column_Ratio2="r2";
    public static String Column_Ratio3="r3";
    public static String Column_Name="name";
    public static double Threshold_Limit=90;

}
