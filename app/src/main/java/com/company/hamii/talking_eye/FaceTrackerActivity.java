/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.company.hamii.talking_eye;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.company.hamii.talking_eye.ui.camera.CameraSourcePreview;
import com.company.hamii.talking_eye.ui.camera.GraphicOverlay;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.android.gms.vision.face.Landmark;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;



public final class FaceTrackerActivity extends AppCompatActivity {
    private static final String TAG = "FaceTracker";

    private CameraSource mCameraSource = null;
    AlertDialog alertDialog;
    AlertDialog.Builder alertDialogBuilder;
    String name;
    SQLiteDatabase db;

    private CameraSourcePreview mPreview;
    private GraphicOverlay mGraphicOverlay;
    static AlertDialog.Builder builder;

    TextToSpeech tts;
    private static final int RC_HANDLE_GMS = 9001;
    int count1,count2;

    List<Integer> marksx=new ArrayList<>();
    List<Integer> marksy=new ArrayList<>();
    List<Integer> typemark=new ArrayList<>();

    List<Integer> marksxSave=new ArrayList<>();
    List<Integer> marksySave=new ArrayList<>();
    List<Integer> typemarkSave=new ArrayList<>();

    List<Double> threshold_ratio_r1=new ArrayList<>();
    List<Double> threshold_ratio_r2=new ArrayList<>();
    List<Double> threshold_ratio_r3=new ArrayList<>();
    List<String> thread_name=new ArrayList<>();

    List<Double> Average=new ArrayList<>();
    String check_var="no";
    int Number_0f_dataSave=0;



    public static Context context;


    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.main);

        alertDialogBuilder =new AlertDialog.Builder(FaceTrackerActivity.this);

        context=FaceTrackerActivity.this;
        db = FaceTrackerActivity.this.openOrCreateDatabase(Constant.DatabaseName,  MODE_PRIVATE, null);

        mPreview = (CameraSourcePreview) findViewById(R.id.preview);
        mGraphicOverlay = (GraphicOverlay) findViewById(R.id.faceOverlay);


        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource();
        }
        count1=0;
        count2=0;


        final MediaPlayer mp = MediaPlayer.create(this,R.raw.spe2);
        if(!mp.isPlaying()) {
            mp.start();
        }
        else {
            mp.stop();

            mp.prepareAsync();
            //mp.release();

        }

        mGraphicOverlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Number_0f_dataSave != 3) {
                    Toast.makeText(FaceTrackerActivity.this, "Save face ", Toast.LENGTH_SHORT).show();
                    marksxSave = marksx;
                    marksySave = marksy;
                    typemarkSave = typemark;
                    check_var = "yes";

                    getName();
                }
                else{
                    Number_0f_dataSave=0;
                    getNumberofdataSave();
                }




            }
        });


        // TODO: Set up the Text To Speech engine.
        TextToSpeech.OnInitListener listener =
                new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(final int status) {
                        if (status == TextToSpeech.SUCCESS) {
                            Log.d("TTS", "Text to speech engine started successfully.");

                            tts.setLanguage(Locale.US);
                        } else {
                            Log.d("TTS", "Error starting the text to speech engine.");
                        }
                    }
                };
        tts = new TextToSpeech(this.getApplicationContext(), listener);



    }

    void getName(){
        LayoutInflater li = LayoutInflater.from(FaceTrackerActivity.this);
        View promptsView = li.inflate(R.layout.name_prompt, null);




        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.editTextDialogUserInput);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // get user input and set it to result
                                // edit text
                                 name=userInput.getText().toString();
                                findLandmarks(marksxSave,marksySave,typemarkSave,name);


                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                            }
                        });

        // create alert dialog
        alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();




    }


    void getFullCoordinate(){

        builder = new AlertDialog.Builder(FaceTrackerActivity.this);
        builder.setTitle("Error! ");
        builder.setMessage("Save Data again because some Data is missing");
        AlertDialog alertDialog ;

        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //alertDialog.dismiss();
            }
        });



        alertDialog = builder.create();
        alertDialog.show();




    }


    void getNumberofdataSave(){

        builder = new AlertDialog.Builder(FaceTrackerActivity.this);
        builder.setTitle("Save! ");
        builder.setMessage("Thank you, Your Data is save");
        AlertDialog alertDialog ;

        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //alertDialog.dismiss();
            }
        });



        alertDialog = builder.create();
        alertDialog.show();




    }



    void getDataAgain(){

        builder = new AlertDialog.Builder(FaceTrackerActivity.this);
        builder.setTitle("Information! ");
        builder.setMessage("Move Camera then Focus on Face again to save more data and then save data again");
        AlertDialog alertDialog ;

        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //alertDialog.dismiss();
            }
        });



        alertDialog = builder.create();
        alertDialog.show();




    }


    public void findLandmarks(List<Integer> marksx, List<Integer> marksy,List<Integer> typemark,String name ){

        int LEFT_EYE=4, RIGHT_EYE=10, MOUTH_BOTTOM=0, LEFT_CHEEK=1, RIGHT_CHEEK=7, LEFT_MOUTH=5, RIGHT_MOUTH=11, NOSE_BASE=6;

        int i1,i2,i3;
        double r1 = 0,r2=0,r3=0;
        int counter_check_type=0;

        if(typemark.contains(LEFT_EYE) && typemark.contains(RIGHT_EYE) && typemark.contains(MOUTH_BOTTOM)){
            i1=typemark.indexOf(LEFT_EYE);
            i2=typemark.indexOf(RIGHT_EYE);
            i3=typemark.indexOf(MOUTH_BOTTOM);

            r1=getRatios(marksx.get(i1),marksy.get(i1),marksx.get(i2),marksy.get(i2),marksx.get(i3),marksy.get(i3));

            System.out.println("Find ration before save 1 "+r1);
            counter_check_type++;
        }
        if(typemark.contains(LEFT_CHEEK) && typemark.contains(RIGHT_CHEEK) && typemark.contains(MOUTH_BOTTOM)){
            i1=typemark.indexOf(LEFT_CHEEK);
            i2=typemark.indexOf(RIGHT_CHEEK);
            i3=typemark.indexOf(MOUTH_BOTTOM);

            r2=getRatios(marksx.get(i1),marksy.get(i1),marksx.get(i2),marksy.get(i2),marksx.get(i3),marksy.get(i3));

            System.out.println("Find ration before save 2 "+r2);
            counter_check_type++;
        }
        if(typemark.contains(LEFT_MOUTH) && typemark.contains(RIGHT_MOUTH) && typemark.contains(NOSE_BASE)){
            i1=typemark.indexOf(LEFT_MOUTH);
            i2=typemark.indexOf(RIGHT_MOUTH);
            i3=typemark.indexOf(NOSE_BASE);

            r3=getRatios(marksx.get(i1),marksy.get(i1),marksx.get(i2),marksy.get(i2),marksx.get(i3),marksy.get(i3));

            System.out.println("Find ration before save 3 "+r3);
            counter_check_type++;
        }

        if(counter_check_type != 3){
            getFullCoordinate();
        }
        if(counter_check_type==3){
            Average.add(r1);
            Average.add(r2);
            Average.add(r3);

            if(Number_0f_dataSave  != 3){
                Number_0f_dataSave++;
                getDataAgain();


            }

        }

        if(Number_0f_dataSave ==3 ) {

            System.out.println("Number_0f_dataSave "+Number_0f_dataSave);
            r1 = (((Average.get(0)*100000) + (Average.get(3)*100000 )+ (Average.get(6)*100000)) / 3);
            r2 = (((Average.get(1)*100000) + (Average.get(4)*100000 )+ (Average.get(7)*100000)) / 3);
            r3 = (((Average.get(2)*100000) + (Average.get(5)*100000 )+ (Average.get(8)*100000)) / 3);


            Average.clear();


            System.out.print("After average r1 " + r1);
            System.out.print("After average r2 " + r2);
            System.out.print("After average r3 " + r3);



            db.execSQL("CREATE TABLE if not exists " + Constant.TableName + " (" +
                    Constant.Column_Name + " TEXT PRIMARY KEY, " +
                    Constant.Column_Ratio1 + " REAL NOT NULL," +
                    Constant.Column_Ratio2 + " REAL NOT NULL," +
                    Constant.Column_Ratio3 + " REAL NOT NULL);"

            );

            ContentValues content = new ContentValues();
            content.put(Constant.Column_Name, name);
            content.put(Constant.Column_Ratio1, String.valueOf(r1));
            content.put(Constant.Column_Ratio2, String.valueOf(r2));
            content.put(Constant.Column_Ratio3, String.valueOf(r3));

            System.out.println("Name in db  " + name);
            System.out.println("Ratio1 in db " + r1);
            System.out.println("Ratio2 in db " + r2);
            System.out.println("Ratio3 in db " + r3);

            db.insert(Constant.TableName, null, content);
        }


    }

    public void check(){

        int LEFT_EYE=4, RIGHT_EYE=10, MOUTH_BOTTOM=0, LEFT_CHEEK=1, RIGHT_CHEEK=7, LEFT_MOUTH=5, RIGHT_MOUTH=11, NOSE_BASE=6;
        int i1,i2,i3;
        double checkRatio1=0,checkRatio2=0,checkRatio3=0;
        double r1_local,r2_Local,r3_Local,threshold,check_threshold_local1=0,check_threshold_local2=0,check_threshold_local3=0;
        String Name_r1 = "UnKnown",Name_r2="b",Name_r3="c",selected_Name = "UnKnown";
        Cursor cr2 = null;
        Cursor cr=null;
        int count=0;
        double threshold_Local=Constant.Threshold_Limit;
        double sum_r1=0;
        double maxRatio=0;


        if(typemark.contains(LEFT_EYE) && typemark.contains(RIGHT_EYE) && typemark.contains(MOUTH_BOTTOM)){
            i1=typemark.indexOf(LEFT_EYE);
            i2=typemark.indexOf(RIGHT_EYE);
            i3=typemark.indexOf(MOUTH_BOTTOM);

            checkRatio1=getRatios(marksx.get(i1),marksy.get(i1),marksx.get(i2),marksy.get(i2),marksx.get(i3),marksy.get(i3));
            checkRatio1=checkRatio1*100000;
            System.out.println("checkRatio1 "+checkRatio1);

        }

        if(typemark.contains(LEFT_CHEEK) && typemark.contains(RIGHT_CHEEK) && typemark.contains(MOUTH_BOTTOM)){
            i1=typemark.indexOf(LEFT_CHEEK);
            i2=typemark.indexOf(RIGHT_CHEEK);
            i3=typemark.indexOf(MOUTH_BOTTOM);

            checkRatio2=getRatios(marksx.get(i1),marksy.get(i1),marksx.get(i2),marksy.get(i2),marksx.get(i3),marksy.get(i3));
            checkRatio2=checkRatio2*100000;
            System.out.println("checkRatio2 "+checkRatio2);


        }
        if(typemark.contains(LEFT_MOUTH) && typemark.contains(RIGHT_MOUTH) && typemark.contains(NOSE_BASE)){
            i1=typemark.indexOf(LEFT_MOUTH);
            i2=typemark.indexOf(RIGHT_MOUTH);
            i3=typemark.indexOf(NOSE_BASE);

            checkRatio3=getRatios(marksx.get(i1),marksy.get(i1),marksx.get(i2),marksy.get(i2),marksx.get(i3),marksy.get(i3));
            checkRatio3=checkRatio3*100000;
            System.out.println("checkRatio3 "+checkRatio3);
        }


            cr2 = null;

            try {
                cr2 = db.rawQuery("select * from sqlite_master WHERE type = ? AND name = ?",  new String[] {"table", Constant.TableName});

            }
            catch (android.database.sqlite.SQLiteException e)
            {
                finish();
            }

            if(cr2 != null) {
                db.execSQL("CREATE TABLE if not exists " + Constant.TableName + " (" +
                        Constant.Column_Name + " TEXT PRIMARY KEY, " +
                        Constant.Column_Ratio1 + " REAL NOT NULL," +
                        Constant.Column_Ratio2 + " REAL NOT NULL," +
                        Constant.Column_Ratio3 + " REAL NOT NULL);"

                );


                System.out.println("checkRatio1 if cr2 null  ");
                System.out.println("Column count " + cr2.getColumnCount());
                cr = db.rawQuery("select * from " + Constant.TableName, null);


                if (cr.moveToFirst()) {
                    System.out.println("checkRatio1 if cr movefirst  ");
                    do {
                        System.out.println("checkRatio1 if cr movenull  ");
                        //r1_local = cr2.getDouble(cr2.getColumnIndex(Constant.Column_Ratio1));

                        if(checkRatio1 != 0) {
                            r1_local = cr.getDouble(cr.getColumnIndex(Constant.Column_Ratio1));
                            threshold = (Math.min(r1_local, checkRatio1) / Math.max(r1_local, checkRatio1)) * 100;

                            sum_r1+=threshold*0.6;
                        }
                        if(checkRatio2 != 0) {
                            r2_Local = cr.getDouble(cr.getColumnIndex(Constant.Column_Ratio2));
                            threshold = (Math.min(r2_Local, checkRatio2) / Math.max(r2_Local, checkRatio2)) * 100;

                            sum_r1+=threshold*0.2;
                        }
                        if(checkRatio3 != 0) {
                            r3_Local = cr.getDouble(cr.getColumnIndex(Constant.Column_Ratio3));
                            threshold = (Math.min(r3_Local, checkRatio3) / Math.max(r3_Local, checkRatio3)) * 100;

                            sum_r1+=threshold*0.2;

                        }

                        if(sum_r1>maxRatio) {
                            Name_r1 = cr.getString(cr.getColumnIndex(Constant.Column_Name));
                            maxRatio=sum_r1;
                            sum_r1=0;
                        }




                       /* threshold_ratio_r1.add(threshold);
                        Name_r1 = cr.getString(cr.getColumnIndex(Constant.Column_Name));
                        thread_name.add(Name_r1);
                        */

                    } while (cr.moveToNext());


                } else {
                    cr.close();
                }


            }
            else{
                cr2.close();
            }



        //Toast.makeText(FaceTrackerActivity.this,"Name: "+selected_Name,Toast.LENGTH_SHORT).show();
        System.out.println("Name: "+Name_r1);
        tts.speak(Name_r1, TextToSpeech.QUEUE_ADD, null, "DEFAULT");

    }

    public double getRatios(int x1, int y1, int x2, int y2, int x3, int y3){

        double d1, d2, mx, my;

        mx= (x1+x2)/2;
        my=(y1+y2)/2;

        double a=(((x1-x2)*(x1-x2))+((y1-y3)*(y1-y2)));
        d1=Math.sqrt(a);

        double b=(((mx-x3)*(mx-x3))+((my-y3)*(my-y3)));
        d2=Math.sqrt(b);

        double ratio= d2/d1;
        return ratio;
//            System.out.println("ratio: "+result);

    }






    private void createCameraSource() {

        Context context = getApplicationContext();
        FaceDetector detector = new FaceDetector.Builder(context)
                .setClassificationType(FaceDetector.ALL_CLASSIFICATIONS)
                .build();

        detector.setProcessor(
                new MultiProcessor.Builder<>(new GraphicFaceTrackerFactory())
                        .build());

        if (!detector.isOperational()) {

            Log.w(TAG, "Face detector dependencies are not yet available.");
        }

        mCameraSource = new CameraSource.Builder(context, detector)
                //.setRequestedPreviewSize(960, 720)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(1280, 1024)
                .setRequestedFps(30.0f)
                .setAutoFocusEnabled(true)
                //.setRequestedFps(30.0f)
                .build();
    }


    @Override
    protected void onResume() {
        super.onResume();

        startCameraSource();
        count1=0;
        count2=0;
        final MediaPlayer mp = MediaPlayer.create(this,R.raw.spe2);
        if(!mp.isPlaying()) {
            mp.start();
        }
        else {
            mp.stop();

            mp.prepareAsync();
            //mp.release();

        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        mPreview.stop();


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCameraSource != null) {
            mCameraSource.release();
        }
    }


    private void startCameraSource() {

        // check that the device has play services available.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }


    private class GraphicFaceTrackerFactory implements MultiProcessor.Factory<Face> {
        @Override
        public Tracker<Face> create(Face face) {
            return new GraphicFaceTracker(mGraphicOverlay);
        }
    }


    private class GraphicFaceTracker extends Tracker<Face> {
        private GraphicOverlay mOverlay;
        private FaceGraphic mFaceGraphic;

        GraphicFaceTracker(GraphicOverlay overlay) {
            mOverlay = overlay;
            mFaceGraphic = new FaceGraphic(overlay);
        }


        @Override
        public void onNewItem(int faceId, Face item) {

            Log.e("face Id", String.valueOf(faceId));
            mFaceGraphic.setId(faceId);
            marksx.clear();
            marksy.clear();
            typemark.clear();
            marksx.add(-1);
            marksy.add(-1);
            typemark.add(-1);

            System.out.println("Left eye"+Landmark.LEFT_EYE);
            System.out.println("Bottom mouth"+Landmark.BOTTOM_MOUTH);
            System.out.println("Right eye"+Landmark.RIGHT_EYE);
            System.out.println("nose "+Landmark.NOSE_BASE);


            for (Landmark landmark : item.getLandmarks()){
                int cz=landmark.getType();
                float cx =(int) (landmark.getPosition().x);
                float cy=(int) (landmark.getPosition().y);
                cx=cx*mOverlay.mWidthScaleFactor;
                cy=cy*mOverlay.mHeightScaleFactor;
                typemark.add(cz);
                marksx.add((int) cx);
                marksy.add((int) cy);
            }

            System.out.println("type "+typemark);
            System.out.println("x "+marksx);
            System.out.println("y "+marksy);

            FaceTrackerActivity Fa=(FaceTrackerActivity) context;
            Fa.check();

        }


        @Override
        public void onUpdate(FaceDetector.Detections<Face> detectionResults, Face face) {
            mOverlay.add(mFaceGraphic);
            mFaceGraphic.updateFace(face);
        }


        @Override
        public void onMissing(FaceDetector.Detections<Face> detectionResults) {
            mOverlay.remove(mFaceGraphic);
        }


        @Override
        public void onDone() {
            mOverlay.remove(mFaceGraphic);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {

            event.startTracking();

            count1++;
            Log.d("Test", "key press " + count1);

            if (count1 == 3) {
                startActivity(new Intent(this, textDetection.class));
            }
           /* if (flag2 == true) {
                flag = false;
            } else {
                flag = true;
                flag2 = false;
            }
            */
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }



    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {

            event.startTracking();

            count2++;
            Log.d("Test", "key press " + count1);

            if (count2 == 3) {
              finishAffinity();
            }
           /* if (flag2 == true) {
                flag = false;
            } else {
                flag = true;
                flag2 = false;
            }
            */
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
