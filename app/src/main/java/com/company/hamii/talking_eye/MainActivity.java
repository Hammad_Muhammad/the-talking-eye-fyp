package com.company.hamii.talking_eye;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import java.util.Locale;

public class MainActivity extends Activity {

    int count1,count2;
    private AudioManager audio;
    TextToSpeech t1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       // audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        count1=0;
        count2=0;


          }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {

            event.startTracking();

            count1++;
            Log.d("Test", "key press " + count1);

            if(count1==3)
            {
                startActivity(new Intent(this,textDetection.class));
            }
           /* if (flag2 == true) {
                flag = false;
            } else {
                flag = true;
                flag2 = false;
            }
            */
            return true;
        }

      /*  switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                audio.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                        AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                audio.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                        AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
                return true;
            default:
                return false;
        }*/
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return super.onKeyUp(keyCode, event);
    }

    public void voice(View view) {
        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                }
            }
        });

        t1.speak("Hello .... Welcome to the Talking Eye", TextToSpeech.QUEUE_FLUSH, null);
        t1.playEarcon("Hello ....  Welcome to the e..",TextToSpeech.QUEUE_FLUSH,null);

    }
}
