package com.company.hamii.talking_eye;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.util.Locale;

public class splashScreen extends Activity {

    TextToSpeech t1;
    public static SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        db = this.openOrCreateDatabase(Constant.DatabaseName,  MODE_PRIVATE, null);
        Cursor cr = db.rawQuery("select * from sqlite_master WHERE type = ? AND name = ?",  new String[] {"table", Constant.TableName});


        final MediaPlayer mp = MediaPlayer.create(this,R.raw.spe1);
        if(!mp.isPlaying()) {
            mp.start();
        }
        else {
            mp.stop();

            mp.prepareAsync();
            //mp.release();

        }
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent i = new Intent(splashScreen.this, FaceTrackerActivity.class);
                startActivity(i);

                finish();
            }
        }, 3000);
    }

}
